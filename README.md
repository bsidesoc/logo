bSidesOC Logo
-------------

This is the Logo repository for [bSidesOC](http://bsidesoc.org).


Our Stylized Bootstrap Logo, and info.
https://bsidesoc.org/community/about/logo/

Image: 
https://bsidesoc.org/community/about/logo/logo-bsidesoc/


Within this repo are several files.
[logo-test.html](logo-test.html)

Please use the [logo-test.html](logo-test.html) as examples for transparency usage with the logo images. Unfortunately, with the background color changes needed this cannot be markdown to my knowledge, which would allow for viewing on Github.

Working Logo Source image the .svg file which is edited with Inkscape.

It is an imported .eps file which was given to us by the main bSides California.

![bSidesOC_Logo_Orange_Black.svg](bSidesOC_Logo_Orange_Black.svg)

![bSidesOC_Logo_Orange_Black.png](bSidesOC_Logo_Orange_Black.png)

[bSidesOC_Logo_Orange_Black.eps](bSidesOC_Logo_Orange_Black.eps)


![bSidesOC_Bootstrap_Style_Logo_Orange_Black.svg](bSidesOC_Bootstrap_Style_Logo_Orange_Black.svg)

![bSidesOC_Bootstrap_Style_Logo_Orange_Black.png](bSidesOC_Bootstrap_Style_Logo_Orange_Black.png)

[bSidesOC_Bootstrap_Style_Logo_Orange_Black.eps](bSidesOC_Bootstrap_Style_Logo_Orange_Black.eps)


![bSidesOC_Bootstrap_Style_Logo_Grayscale.svg](bSidesOC_Bootstrap_Style_Logo_Grayscale.svg)

![bSidesOC_Bootstrap_Style_Logo_Grayscale.png](bSidesOC_Bootstrap_Style_Logo_Grayscale.png)

[bSidesOC_Bootstrap_Style_Logo_Grayscale.eps](bSidesOC_Bootstrap_Style_Logo_Grayscale.eps)


![bSidesOC_Icon_Logo_Orange_Black.svg](bSidesOC_Icon_Logo_Orange_Black.svg)

![bSidesOC_Icon_Logo_Orange_Black.png](bSidesOC_Icon_Logo_Orange_Black.png)

[bSidesOC_Icon_Logo_Orange_Black.eps](bSidesOC_Icon_Logo_Orange_Black.eps)


![bSidesOC_QR_Icon_Logo_Orange_Black.svg](bSidesOC_QR_Icon_Logo_Orange_Black.svg)

![bSidesOC_QR_Icon_Logo_Orange_Black.png](bSidesOC_QR_Icon_Logo_Orange_Black.png)

[bSidesOC_QR_Icon_Logo_Orange_Black.eps](bSidesOC_QR_Icon_Logo_Orange_Black.eps)






## 2013 Event 

Banner and t-shirts
--------------

![banner/2013-bSidesOC/BannerPrewvieuz.png](banner/2013-bSidesOC/BannerPrewvieuz.png)

[banner/2013-bSidesOC/BannerEPS.eps](banner/2013-bSidesOC/BannerEPS.eps)


[t-shirt/final/FINAL_2013-07_TEST_3_bSidesOC_T-Shirt_Unicorns_Logo_Orange_Black.eps](t-shirt/final/FINAL_2013-07_TEST_3_bSidesOC_T-Shirt_Unicorns_Logo_Orange_Black.eps)

![t-shirt/final/FINAL_2013-09_BSIDES OC T-SHIRT LAYOUT.jpg](t-shirt/final/FINAL_2013-09_BSIDES OC T-SHIRT LAYOUT.jpg)

[t-shirt/final/FINAL_2013-09_bSidesOC_T-Shirt_with_Sponsors_Unicorns_Logo_Orange_Black.eps](t-shirt/final/FINAL_2013-09_bSidesOC_T-Shirt_with_Sponsors_Unicorns_Logo_Orange_Black.eps)

![t-shirt/final/FINAL_2013-09_bSidesOC_T-Shirt_with_Sponsors_Unicorns_Logo_Orange_Black.png](t-shirt/final/FINAL_2013-09_bSidesOC_T-Shirt_with_Sponsors_Unicorns_Logo_Orange_Black.png)


QR & Tracking
----------------


<img src="http://go.bsidesoc.org/1.qr.png" style="max-width:200px;border:0;" width="200" />


<img src="https://bsidesoc.org/piwik/piwik.php?id=3&rec=1" />


