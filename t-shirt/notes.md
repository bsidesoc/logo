
# eps file steps

1. render svg as a bitmap, png.
2. Import it as new layer. (this isn't great. should be better way!)
3. Color trace it.
4. Then export as EPS.


## another way?

1. select each image
2. Path -> Trace Bitmap for each
3. Save a Copy in .eps

### another nother way

1. export work as bitmap in high resolution
2. OPEN this file using Inkscape.
3. Create a new layer from the root.
4. Select the image.
5. Path -> Trace Bitmap
6. Make sure "remove background" is checked
7. Click OK. wait a few minutes.
8. Drag the new trace off to the side to reveal the bitmap image.
9. delete the bitmap
10. make sure your color trace is on it's own layer.
11. Save a copy as EPS

